---
- hosts: ccce
  become: true
  roles:
    - role: ics-ans-role-ca-trust
    - role: ics-ans-role-traefik
      tags:
        - ce-deploy-backend
        - ce-deploy-ui
  collections:
    - awx.awx
  tasks:
    - name: Create AWX configuration
      tags:
        - ce-deploy-backend
        - ce-deploy-ui
      block:
        - name: Create notification template
          awx.awx.tower_notification_template:
            name: ccce-ioc-notifications{{ ccce_awx_suffix }}
            tower_host: "{{ ccce_awx_host }}"
            tower_username: "{{ ccce_awx_admin_username }}"
            tower_password: "{{ ccce_awx_admin_password }}"
            validate_certs: "{{ ccce_awx_validate_certs }}"
            organization: "{{ ccce_organization | default('Default') }}"
            notification_type: webhook
            notification_configuration:
              url: "{{ inventory_hostname }}/api/v1/awx/jobs"
              headers:
                CCCE-TOKEN: "{{ ccce_awx_notification_token }}"

        - name: Create AWX Project
          awx.awx.tower_project:
            name: "{{ ccce_awx_project_name }}"
            tower_host: "{{ ccce_awx_host }}"
            tower_username: "{{ ccce_awx_admin_username }}"
            tower_password: "{{ ccce_awx_admin_password }}"
            validate_certs: "{{ ccce_awx_validate_certs }}"
            organization: "{{ ccce_organization | default('Default') }}"
            scm_delete_on_update: "{{ ccce_awx_project_scm_delete_on_update | default('false') }}"
            scm_type: "{{ ccce_awx_project_scm_type | default(omit) }}"
            scm_url: "{{ ccce_awx_project_scm_url | default(omit) }}"
            scm_branch: "{{ ccce_ioc_deploy_playbook_branch | default(omit) }}"
            allow_override: true
            scm_update_on_launch: "{{ ccce_awx_project_scm_update_on_launch | default(omit) }}"
            local_path: "{{ ccce_awx_project_local_path | default(omit) }}"

        - name: Create AWX inventory
          awx.awx.tower_inventory:
            name: "{{ ccce_awx_inventory_name }}"
            tower_host: "{{ ccce_awx_host }}"
            tower_username: "{{ ccce_awx_admin_username }}"
            tower_password: "{{ ccce_awx_admin_password }}"
            validate_certs: "{{ ccce_awx_validate_certs }}"
            organization: "{{ ccce_organization | default('Default') }}"

        - name: Create project to read IOC inventory
          awx.awx.tower_project:
            name: "{{ ccce_awx_project_name }}-inventory"
            tower_host: "{{ ccce_awx_host }}"
            tower_username: "{{ ccce_awx_admin_username }}"
            tower_password: "{{ ccce_awx_admin_password }}"
            validate_certs: "{{ ccce_awx_validate_certs }}"
            organization: "{{ ccce_organization | default('Default') }}"
            scm_delete_on_update: "{{ ccce_awx_project_scm_delete_on_update | default('false') }}"
            scm_type: git
            scm_url: "{{ ccce_awx_ioc_inventory_scm_url | default(omit) }}"
            scm_branch: "{{ ccce_awx_ioc_inventory_branch | default(omit) }}"
            scm_update_on_launch: "{{ ccce_awx_project_scm_update_on_launch | default(omit) }}"
            allow_override: true
            local_path: "{{ ccce_awx_project_local_path | default(omit) }}"

        - name: Add inventory source to AWX inventory
          awx.awx.tower_inventory_source:
            name: "ce-deploy-inventory-source"
            tower_host: "{{ ccce_awx_host }}"
            tower_username: "{{ ccce_awx_admin_username }}"
            tower_password: "{{ ccce_awx_admin_password }}"
            validate_certs: "{{ ccce_awx_validate_certs }}"
            inventory: "{{ ccce_awx_inventory_name }}"
            source: "scm"
            source_project: "{{ ccce_awx_project_name }}-inventory"
            source_path: "{{ ccce_awx_ioc_inventory_script_name }}"
            source_vars:
              CE_DEPLOY_URL: "{{ ccce_server_address }}"
            update_on_launch: true

        - name: Add netbox inventory source to AWX inventory
          awx.awx.tower_inventory_source:
            name: "netbox-inventory-source"
            tower_host: "{{ ccce_awx_host }}"
            tower_username: "{{ ccce_awx_admin_username }}"
            tower_password: "{{ ccce_awx_admin_password }}"
            validate_certs: "{{ ccce_awx_validate_certs }}"
            inventory: "{{ ccce_awx_inventory_name }}"
            source: "tower"
            credential: "{{ ccce_awx_netbox_read_credential }}"
            source_vars:
              inventory_id: "{{ ccce_awx_netbox_inventory_id }}"
            update_on_launch: true
          when: ccce_awx_netbox_inventory_id is defined

        - name: Create AWX Job template
          awx.awx.tower_job_template:
            name: "{{ ccce_awx_job_template_name }}"
            tower_host: "{{ ccce_awx_host }}"
            tower_username: "{{ ccce_awx_admin_username }}"
            tower_password: "{{ ccce_awx_admin_password }}"
            validate_certs: "{{ ccce_awx_validate_certs }}"
            description: "Deploy IOCs via CE-deploy{{ ccce_awx_suffix }}"
            organization: "{{ ccce_organization | default('Default') }}"
            job_type: run
            timeout: "{{ ccce_awx_job_template_timeout | default(omit) }}"
            inventory: "{{ ccce_awx_inventory_name }}"
            project: "{{ ccce_awx_project_name }}"
            scm_branch: "{{ ccce_ioc_deploy_playbook_job_template_branch | default(omit) }}"
            playbook: playbook.yml
            credentials: "{{ ccce_awx_machine_credential | default(omit) }}"
            extra_vars: {}
            use_fact_cache: true
            notification_templates_error:
              ["ccce-ioc-notifications{{ ccce_awx_suffix }}"]
            notification_templates_started:
              ["ccce-ioc-notifications{{ ccce_awx_suffix }}"]
            notification_templates_success:
              ["ccce-ioc-notifications{{ ccce_awx_suffix }}"]

        - name: Create AWX roles (job template)
          awx.awx.tower_role:
            tower_host: "{{ ccce_awx_host }}"
            tower_username: "{{ ccce_awx_admin_username }}"
            tower_password: "{{ ccce_awx_admin_password }}"
            validate_certs: "{{ ccce_awx_validate_certs }}"
            user: "{{ ccce_awx_technical_user_username }}"
            job_templates:
              - "{{ ccce_awx_job_template_name }}"
            role: "{{ item }}"
          loop: "{{ ccce_awx_job_template_roles | default([]) }}"

        - name: Create AWX roles (inventory)
          awx.awx.tower_role:
            tower_host: "{{ ccce_awx_host }}"
            tower_username: "{{ ccce_awx_admin_username }}"
            tower_password: "{{ ccce_awx_admin_password }}"
            validate_certs: "{{ ccce_awx_validate_certs }}"
            user: "{{ ccce_awx_technical_user_username }}"
            inventories:
              - "{{ ccce_awx_inventory_name }}"
            role: "{{ item }}"
          loop: "{{ ccce_awx_inventory_roles | default([]) }}"

        - name: Create AWX roles (project)
          awx.awx.tower_role:
            tower_host: "{{ ccce_awx_host }}"
            tower_username: "{{ ccce_awx_admin_username }}"
            tower_password: "{{ ccce_awx_admin_password }}"
            validate_certs: "{{ ccce_awx_validate_certs }}"
            user: "{{ ccce_awx_technical_user_username }}"
            projects:
              - "{{ ccce_awx_project_name }}"
            role: "{{ item }}"
          loop: "{{ ccce_awx_project_roles | default([]) }}"
      when: ccce_awx_create_config

    - name: Create container network
      docker_network:
        name: ccce
        state: present
      tags:
        - ce-deploy-backend
        - ce-deploy-ui

    # We have left these two as ccce-database since renaming volumes isn't apparently a thing
    - name: Create database container
      docker_container:
        name: ccce-database
        image: postgres:13.3
        purge_networks: "yes"
        networks:
          - name: ccce
        state: started
        restart_policy: always
        published_ports:
          - 127.0.0.1:5432:5432
        volumes:
          - ccce-database:/var/lib/postgresql/data/pgdata
        env:
          POSTGRES_DB: "{{ ce_deploy_database_name }}"
          POSTGRES_USER: "{{ ce_deploy_database_username }}"
          POSTGRES_PASSWORD: "{{ ce_deploy_database_password }}"
          PGDATA: /var/lib/postgresql/data/pgdata
      tags:
        - ce-deploy-backend

    - name: Create backend application container
      docker_container:
        name: ce-deploy-backend
        image: "registry.esss.lu.se/ccce/dev/ce-deploy-backend:{{ ce_deploy_backend_container_image_tag }}"
        pull: true
        purge_networks: true
        networks:
          - name: ccce
          - name: "{{ traefik_network }}"
        state: started
        restart_policy: always
        env:
          AES_KEY: "{{ ccce_aes_key | default(omit) }}"
          API_PATH: "{{ ccce_api_path | default(omit) }}"
          AWX_HOST: "{{ ccce_awx_host | default(omit) }}"
          AWX_TOKEN: "{{ ccce_awx_token | default(omit) }}"
          AWX_JOB_TEMPLATE_NAME: "{{ ccce_awx_job_template_name | default(omit) }}"
          AWX_JOB_TEMPLATE_HOST_FILTER: "{{ ccce_awx_job_template_host_pattern_restriction | default(omit) }}"
          AWX_INVENTORY_ID: "{{ ccce_awx_netbox_inventory_id | default(omit) }}"
          BATCH_JOB_MAX_SIZE: "{{ ce_deploy_batch_job_max_size | default(omit) }}"
          CACHE_EXP_IN_SEC: "{{ ccce_cache_exp_in_sec | default(omit) }}"
          CCCE_DB_URL: "jdbc:postgresql://ccce-database:5432/{{ ce_deploy_database_name }}"
          CCCE_DB_USERNAME: "{{ ce_deploy_database_username }}"
          CCCE_DB_PASSWORD: "{{ ce_deploy_database_password }}"
          CCCE_DB_MAX_POOL_SIZE: "{{ ce_deploy_database_max_pool_size | default(omit) }}"
          CCCE_DB_MIN_POOL_SIZE: "{{ ce_deploy_database_min_pool_size | default(omit) }}"
          CCCCE_DB_POOL_NAME: "{{ ce_deploy_database_pool_name | default(omit) }}"
          CCCE_DB_KEEPALIVE_INT: "{{ ce_deploy_database_keepalive_int | default(omit) }}"
          CCCE_DB_CONN_TIMEOUT: "{{ ce_deploy_database_conn_timeout | default(omit) }}"
          CCCE_DB_IDLE_TIMEOUT: "{{ ce_deploy_database_idle_timeout | default(omit) }}"
          CCCE_DB_MAX_LIFETIME: "{{ ce_deploy_database_max_lifetime | default(omit) }}"
          CHANNELFINDER_HOST: "{{ ccce_channelfinder_host | default(omit) }}"
          GITLAB_ALLOWED_GROUP_ID: "{{ gitlab_allowed_group_id | default(omit) }}"
          GITLAB_HOST: "{{ ccce_gitlab_host | default(omit) }}"
          GITLAB_CLIENT_APP_ID: "{{ ccce_gitlab_client_app_id }}"
          GITLAB_CLIENT_APP_SECRET: "{{ ccce_gitlab_client_app_secret }}"
          GITLAB_TECHNICAL_USER_TOKEN: "{{ ccce_gitlab_technical_user_token | default(omit) }}"
          GITLAB_SUBGROUP_TO_GENERATE: "{{ ce_deploy_subgroup_to_generate_repo | default(omit) }}"
          GRAYLOG_HOST: "{{ ccce_graylog_host | default(omit) }}"
          GRAYLOG_TOKEN: "{{ ccce_graylog_token }}"
          HOST_MAX_CAPACITY: "{{ ccce_host_max_capacity | default(omit) }}"
          IOC_METADATA_PATH: "{{ ccce_ioc_metadata_path | default(omit) }}"
          JWT_SECRET: "{{ ccce_jwt_secret | default(omit) }}"
          JWT_EXP_MIN: "{{ ccce_jwt_exp_min | default(omit) }}"
          LOG_CONFIG_FILE: "{{ ccce_log_config_file | default(omit) }}"
          NETBOX_CACHE_EXP_IN_SEC: "{{ ccce_netbox_cache_exp_in_sec | default(omit) }}"
          NETBOX_HOST: "{{ ccce_netbox_host | default(omit) }}"
          NETBOX_HOST_GROUPS_FILTER: "{{ ccce_netbox_host_groups_filter | default(omit) }}"
          NETBOX_TOKEN: "{{ ccce_netbox_token | default(omit) }}"
          OPENAPI_SERVER: "{{ ccce_openapi_server | default(omit) }}"
          OPENAPI_DEV_SERVER: "{{ ccce_openapi_dev_server | default(omit) }}"
          OPENAPI_TEST_SERVER: "{{ ccce_openapi_test_server | default(omit) }}"
          PROMETHEUS_HOST: "{{ ccce_prometheus_host | default(omit) }}"
          RBAC_SERVER_ADDRESS: "{{ ccce_rbac_server_address | default(omit) }}"
          RESPONSE_MAX_LIMIT: "{{ ccce_response_max_limit | default(omit) }}"
          SWAGGER_PATH: "{{ ccce_swagger_path | default(omit) }}"
          ALLOW_UNTRUSTED_CERTS: "{{ ccce_allow_untrusted_certs | default(omit) | string }}"
        volumes:
          - /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem:/etc/ssl/certs/ca-certificates.crt:ro
          - /etc/pki/ca-trust/extracted/java/cacerts:/etc/ssl/certs/java/cacerts:ro
        labels:
          traefik.enable: "true"
          traefik.backend: ce-deploy-backend
          traefik.port: "8080"
          traefik.frontend.rule: "{{ ce_deploy_backend_frontend_rule }}"
          traefik.docker.network: "{{ traefik_network }}"
      tags:
        - ce-deploy-backend

    - name: Create frontend application configuration directory
      file:
        state: directory
        path: /etc/ccce/ce-deploy-ui
        mode: 0644
        owner: root
        group: root
      tags:
        - ce-deploy-ui

    - name: Create frontend application configuration
      copy:
        dest: /etc/ccce/ce-deploy-ui/config.js
        mode: 0644
        owner: root
        group: root
        content: |
          SERVER_ADDRESS='{{ ccce_server_address | default(omit) }}'
          API_BASE_ENDPOINT='{{ ccce_api_base_endpoint | default('/api/spec') }}'
          SWAGGER_UI_URL='{{ ccce_swagger_ui_endpoint | default('/api/docs') }}'
          TOKEN_RENEW_INTERVAL='{{ ccce_token_renew_interval | default(180000) }}'
          NETBOX_ADDRESS='{{ ccce_netbox_host | default('https://netbox.esss.lu.se') }}'
          ENVIRONMENT='{{ ce_deploy_ui_web_environment | default('PROD') }}'
          ENVIRONMENT_TITLE='{{ ce_deploy_ui_web_environment_title | default('') }}'
          FRONTEND_VERSION='{{ ce_deploy_ui_container_image_tag }}'
          NAMING_ADDRESS='{{ ccce_naming_base_url | default('https://naming.esss.lu.se') }}'
          SUPPORT_URL='{{ ce_support_url }}'
      notify:
        - Restart frontend application container
      tags:
        - ce-deploy-ui

    - name: Create frontend application container
      docker_container:
        name: ce-deploy-ui
        image: "registry.esss.lu.se/ccce/dev/ce-deploy-ui:{{ ce_deploy_ui_container_image_tag }}"
        pull: true
        purge_networks: true
        networks:
          - name: ccce
          - name: "{{ traefik_network }}"
        state: started
        restart_policy: always
        volumes:
          - /etc/ccce/ce-deploy-ui/config.js:/usr/share/nginx/html/config.js:ro
        labels:
          traefik.enable: "true"
          traefik.backend: ce-deploy-ui
          traefik.port: "8080"
          traefik.frontend.rule: "{{ ce_deploy_ui_frontend_rule }}"
          traefik.docker.network: "{{ traefik_network }}"
      tags:
        - ce-deploy-ui

    - name: Wait for frontend to be registered with traefik
      uri:
        url: http://localhost:8080/api
        return_content: "yes"
        status_code: 200
        body_format: json
      register: traefik_content
      until: "'backend-ce-deploy-ui' in traefik_content | json_query('json.docker.frontends.*.backend')"
      retries: 60
      delay: 1
      tags:
        - ce-deploy-ui

  handlers:
    - name: Restart frontend application container
      docker_container:
        name: ce-deploy-ui
        state: started
        restart: true

- hosts: ce_template
  become: true
  roles:
    - role: ics-ans-role-repository
      tags:
        - ce-template-backend
        - ce-template-ui
    - role: ics-ans-role-traefik
      tags:
        - ce-template-backend
        - ce-template-ui
  tasks:
    - name: Create container network
      docker_network:
        name: ce-template
        state: present
      tags:
        - ce-template-backend
        - ce-template-ui

    - name: Create database container
      docker_container:
        name: ce-template-database
        image: postgres:14.5
        purge_networks: "yes"
        networks:
          - name: ce-template
        state: started
        restart_policy: always
        published_ports:
          - 127.0.0.1:5432:5432
        volumes:
          - ce-template-database:/var/lib/postgresql/data/pgdata
        env:
          POSTGRES_DB: "{{ ce_template_database_name }}"
          POSTGRES_USER: "{{ ce_template_database_username }}"
          POSTGRES_PASSWORD: "{{ ce_template_database_password }}"
          PGDATA: /var/lib/postgresql/data/pgdata
      tags:
        - ce-template-backend

    - name: Create backend application container
      docker_container:
        name: ce-template-backend
        image: "registry.esss.lu.se/ccce/dev/ce-template-backend:{{ ce_template_backend_container_image_tag }}"
        pull: true
        purge_networks: true
        networks:
          - name: ce-template
          - name: "{{ traefik_network }}"
        state: started
        restart_policy: always
        env:
          GITLAB_HOST: "{{ ccce_gitlab_host }}"
          GITLAB_TECHNICAL_USER_TOKEN: "{{ ce_template_gitlab_technical_user_token }}"
          GITLAB_ALLOWED_OUTPUT_GROUP_ID: "{{ ce_template_gitlab_allowed_output_group_id | default(omit) }}"
          GITLAB_ALLOWED_TEMPLATE_GROUP_ID: "{{ ce_template_gitlab_allowed_template_group_id | default(omit) }}"
          GITLAB_ALLOWED_CONFIG_GROUP_ID: "{{ ce_template_gitlab_allowed_config_group_id | default(omit) }}"
          GITLAB_ALLOWED_EXAMPLE_GROUP_ID: "{{ ce_template_gitlab_allowed_example_group_id | default(omit) }}"
          CACHE_EXP_IN_SEC: "{{ ce_template_cache_exp_in_sec | default(6) }}"
          OPENAPI_SERVER: "{{ ce_template_openapi_server | default('http://localhost:8080') }}"
          API_PATH: "{{ ce_template_api_path }}"
          SWAGGER_PATH: "{{ ce_template_swagger_path }}"
          CE_TEMPLATE_DB_URL: "jdbc:postgresql://ce-template-database:5432/{{ ce_template_database_name }}"
          CE_TEMPLATE_DB_USERNAME: "{{ ce_template_database_username }}"
          CE_TEMPLATE_DB_PASSWORD: "{{ ce_template_database_password }}"
          CE_TEMPLATE_DB_MAX_POOL_SIZE: "{{ ce_template_database_max_pool_size | default(omit) }}"
          CE_TEMPLATE_DB_MIN_POOL_SIZE: "{{ ce_template_database_min_pool_size | default(omit) }}"
          CE_TEMPLATE_DB_POOL_NAME: "{{ ce_template_database_pool_name | default(omit) }}"
          CE_TEMPLATE_DB_KEEPALIVE_INT: "{{ ce_template_database_keepalive_int | default(omit) }}"
          CE_TEMPLATE_DB_CONN_TIMEOUT: "{{ ce_template_database_conn_timeout | default(omit) }}"
          CE_TEMPLATE_DB_IDLE_TIMEOUT: "{{ ce_template_database_idle_timeout | default(omit) }}"
          CE_TEMPLATE_DB_MAX_LIFETIME: "{{ ce_template_database_max_lifetime | default(omit) }}"
          RESPONSE_MAX_LIMIT: "{{ ce_template_response_max_limit | default(omit) }}"
          PING_CONFIG_URI: "{{ ce_template_ping_config_url | default(omit) }}"
          PING_CLIENT_ID: "{{ ce_template_ping_client_id | default(omit) }}"
          PING_CLIENT_SECRET: "{{ ce_template_ping_client_secret | default(omit) }}"
          PING_REDIRECT_URL: "{{ ce_template_ping_redirect_url | default(omit) }}"
          PING_SCOPE: "{{ ce_template_ping_scope | default(omit) }}"
          PING_GRANT_TYPE: "{{ ce_template_ping_grant_type | default(omit) }}"
          UI_BASEURL: "{{ ce_template_ui_baseurl | default(omit) }}"
          JWT_SECRET: "{{ ce_template_jwt_secret | default(omit) }}"
          JWT_EXP_MIN: "{{ ce_template_jwt_exp_min | default(omit) }}"
          CE_TEMPLATE_HOST_ADDRESS: "{{ ce_template_backend_host_address | default(omit) }}"
        labels:
          traefik.enable: "true"
          traefik.backend: ce-template-backend
          traefik.port: "8080"
          traefik.frontend.rule: "{{ ce_template_backend_frontend_rule }}"
          traefik.docker.network: "{{ traefik_network }}"
        volumes:
          - /etc/pki/ca-trust/extracted/java/cacerts:/opt/java/openjdk/lib/security/cacerts:ro
      tags:
        - ce-template-backend

    - name: Create frontend application configuration directory
      file:
        state: directory
        path: /etc/ccce/ce-template-ui
        mode: 0644
        owner: root
        group: root
      tags:
        - ce-template-ui

    - name: Create frontend application configuration
      copy:
        dest: /etc/ccce/ce-template-ui/config.js
        mode: 0644
        owner: root
        group: root
        content: |
          SERVER_ADDRESS='{{ ce_template_server_address | default(omit) }}'
          ENVIRONMENT_TITLE='{{ ce_template_environment_title | default('') }}'
          API_BASE_ENDPOINT='{{ ce_template_api_base_endpoint | default('/api/spec') }}'
          TOKEN_RENEW_INTERVAL='{{ ce_template_token_renew_interval | default(180000) }}'
          SWAGGER_UI_URL='{{ ce_template_swagger_ui_endpoint | default('/api/docs') }}'
          TEMPLATE_FRONTEND_VERSION='{{ ce_template_ui_container_image_tag }}'
          SUPPORT_URL='{{ ce_support_url }}'
          BE_BASE='{{ ce_template_server_address }}'
          API_LOGIN='{{ ce_template_login_url }}'
          API_LOGOUT='{{ ce_template_logout_url }}'
      notify:
        - Restart frontend application container
      tags:
        - ce-template-ui

    - name: Create frontend application container
      docker_container:
        name: ce-template-ui
        image: "registry.esss.lu.se/ccce/dev/ce-template-ui:{{ ce_template_ui_container_image_tag }}"
        pull: true
        purge_networks: true
        networks:
          - name: ce-template
          - name: "{{ traefik_network }}"
        state: started
        restart_policy: always
        volumes:
          - /etc/ccce/ce-template-ui/config.js:/usr/share/nginx/html/config.js:ro
        labels:
          traefik.enable: "true"
          traefik.backend: ce-template-ui
          traefik.port: "8080"
          traefik.frontend.rule: "{{ ce_template_ui_frontend_rule }}"
          traefik.docker.network: "{{ traefik_network }}"
      tags:
        - ce-template-ui

    - name: Wait for frontend to be registered with traefik
      uri:
        url: http://localhost:8080/api
        return_content: "yes"
        status_code: 200
        body_format: json
      register: traefik_content
      until: "'backend-ce-template-ui' in traefik_content | json_query('json.docker.frontends.*.backend')"
      retries: 60
      delay: 1
      tags:
        - ce-template-ui

  handlers:
    - name: Restart frontend application container
      docker_container:
        name: ce-template-ui
        state: started
        restart: true

- hosts: ce_naming
  become: true
  roles:
    - role: ics-ans-role-repository
      tags:
        - ce-naming-backend
        - ce-naming-ui
    - role: ics-ans-role-traefik
      tags:
        - ce-naming-backend
        - ce-naming-ui
  tasks:
    - name: Create container network
      docker_network:
        name: ce-naming
        state: present
      tags:
        - ce-naming-backend
        - ce-naming-ui

    - name: Create database container
      docker_container:
        name: ce-naming-database
        image: postgres:9.6.7
        purge_networks: "yes"
        networks:
          - name: ce-naming
        state: started
        restart_policy: always
        published_ports:
          - 127.0.0.1:5432:5432
        volumes: "{{ ce_naming_db_volumes }}"
        env:
          POSTGRES_DB: "{{ ce_naming_database_name }}"
          POSTGRES_USER: "{{ ce_naming_database_username }}"
          POSTGRES_PASSWORD: "{{ ce_naming_database_password }}"
          PGDATA: /var/lib/postgresql/data/pgdata
      tags:
        - ce-naming-backend

    - name: Create backend application container
      docker_container:
        name: ce-naming-backend
        image: "registry.esss.lu.se/ccce/dev/ce-naming-backend:{{ ce_naming_backend_container_image_tag }}"
        pull: true
        purge_networks: true
        networks:
          - name: ce-naming
          - name: "{{ traefik_network }}"
        state: started
        restart_policy: always
        env:
          server.port: "8080"
          server.use-forward-headers: "true"
          server.forward-headers-strategy: NATIVE
          spring.datasource.url: "jdbc:postgresql://ce-naming-database:5432/{{ ce_naming_database_name }}"
          spring.datasource.username: "{{ ce_naming_database_username }}"
          spring.datasource.password: "{{ ce_naming_database_password }}"
        labels:
          traefik.enable: "true"
          traefik.backend: ce-template-backend
          traefik.port: "8080"
          traefik.frontend.rule: "{{ ce_naming_backend_frontend_rule }}"
          traefik.docker.network: "{{ traefik_network }}"
        volumes:
          - /etc/pki/ca-trust/extracted/java/cacerts:/opt/java/openjdk/lib/security/cacerts:ro
      tags:
        - ce-naming-backend

    - name: Create frontend application configuration directory
      file:
        state: directory
        path: /etc/ccce/ce-naming-ui
        mode: 0644
        owner: root
        group: root
      tags:
        - ce-naming-ui

    - name: Create frontend application configuration
      copy:
        dest: /etc/ccce/ce-naming-ui/config.js
        mode: 0644
        owner: root
        group: root
        content: |
          SERVER_ADDRESS='{{ ce_naming_server_address | default(omit) }}'
          ENVIRONMENT_TITLE='{{ ce_naming_environment_title | default('') }}'
          API_BASE_ENDPOINT='{{ ce_naming_api_base_endpoint | default('/api/spec') }}'
          TOKEN_RENEW_INTERVAL='{{ ce_naming_token_renew_interval | default(180000) }}'
          SWAGGER_UI_URL='{{ ce_naming_swagger_ui_endpoint | default('/api/docs') }}'
          FRONTEND_VERSION='{{ ce_naming_ui_container_image_tag }}'
          SUPPORT_URL='{{ ce_support_url }}'
      notify:
        - Restart frontend application container
      tags:
        - ce-naming-ui

    - name: Create frontend application container
      docker_container:
        name: ce-naming-ui
        image: "registry.esss.lu.se/ccce/dev/ce-naming-ui:{{ ce_naming_ui_container_image_tag }}"
        pull: true
        purge_networks: true
        networks:
          - name: ce-naming
          - name: "{{ traefik_network }}"
        state: started
        restart_policy: always
        volumes:
          - /etc/ccce/ce-naming-ui/config.js:/usr/share/nginx/html/config.js:ro
        labels:
          traefik.enable: "true"
          traefik.backend: ce-naming-ui
          traefik.port: "8080"
          traefik.frontend.rule: "{{ ce_naming_ui_frontend_rule }}"
          traefik.docker.network: "{{ traefik_network }}"
      tags:
        - ce-naming-ui

    - name: Wait for frontend to be registered with traefik
      uri:
        url: http://localhost:8080/api
        return_content: "yes"
        status_code: 200
        body_format: json
      register: traefik_content
      until: "'backend-ce-naming-ui' in traefik_content | json_query('json.docker.frontends.*.backend')"
      retries: 60
      delay: 1
      tags:
        - ce-naming-ui

  handlers:
    - name: Restart frontend application container
      docker_container:
        name: ce-naming-ui
        state: started
        restart: true
